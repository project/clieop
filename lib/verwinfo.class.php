<?php
class verwinfo {
  var $_file_tree = array();
  var $_record_defs;
  var $_file_handle;
  var $_current_line;
  var $_error;
  var $_logger;

  function process_file( $file_name){
    $this->_record_defs = new verwinfo_record_defs();

    $this->_file_handle = @fopen( $file_name, "r");

    // Fill first line
    $this->read_line();
    $this->parse_bestand();

    fclose($this->_file_handle);
  }

  function parse_bestand( ){
    $result = array();

    if( !$this->process_record($result, 'bestandsvoorlooprecord')){
      return;
    }

    $data = $this->parse_batches();
    if( $this->test_error()){
      return;
    }
    $batches = $data;

    if( !$this->process_record($result, 'bestandssluitrecord')){
      return;
    }

    $result = $this->zip_fields( $result);
    $result['batches'] = $batches;

    $this->_file_tree = $result;
  }

  function parse_batches(){
    $batches = array();

    while ($this->test_record('batch_voorlooprecord_1')) {
      $batch = array();
      if( !$this->process_record($batch, 'batch_voorlooprecord_1')){
        return;
      }

      $batch_soort = $batch['batch_voorlooprecord_1']['batchsoort'];

      if( !$this->process_record($batch, 'batch_voorlooprecord_2')){
        return;
      }

      if( $batch_soort != 'A'){
        $this->set_error('Unsupported batchsoort: '. $batch_soort);
        return;
      }

      $posten= $this->parse_posten_A();
      if( $this->test_error()) {
        return;
      }

      if( !$this->process_record($batch, 'batch_sluitrecord')){
        return;
      }
      $batch = $this->zip_fields( $batch);
      $batch['posten'] = $posten;
      $batches[] = $batch;
    }
    return $batches;
  }

  function parse_posten_A(){
    $result= array();

    while ($this->test_record('post_record_1')) {
      $record = array();
      if( !$this->process_record($record, 'post_record_1')){
        return;
      }

      if( $this->test_record('eurorecord')){
        if( !$this->process_record($record, 'eurorecord')){
          return;
        }
      }


      if( !$this->process_record($record, 'post_record_2')){
        return;
      }

      $omschrijving = array();
      while( $this->test_record('omschrijving_record')){
        if( !$this->process_record($omschrijving, 'omschrijving_record')){
          return;
        }

        $tekst = $omschrijving['omschrijving_record']['omschrijving'];

        $record['omschrijving_record']['omschrijving'] .= "\n" . $tekst;
        $record['omschrijving_record']['omschrijving'] = trim( $record['omschrijving_record']['omschrijving']);
      }

      if( !$this->process_record($record, 'bankinforecord')){
        return;
      }

      if( !$this->process_record($record, 'equens_record_1')){
        return;
      }

      if( !$this->process_record($record, 'equens_record_2')){
        return;
      }

      $result[] = $this->zip_fields( $record);
    }
    return $result;
  }
  
  function get_records_for_batch( $index){
    $batch = $this->_file_tree['batches'][$index]['posten'];

    $result= array();
    foreach( $batch as $record){
      $result[]= $record;
    }
    
    return $result;
  }

  function zip_fields( $record){
    $result = array();
    foreach($record as $fieldset){
      foreach( $fieldset as $field_key => $field_value){
        $result[$field_key] = $field_value;
      }
    }
    return $result;
  }

  function process_record( &$result, $type){
    $this->log( 'process_record :' . $type );
    $data= $this->read_record($type);
    if( $this->test_error()) {
      return FALSE;
    }
    $result[$type] = $data;
    return TRUE;
  }

  /**
   * Test record for specific type
   *
   * @param $type type to test te record for
   * @return TRUE | FALSE indicating record is of specified type
   */
  function test_record( $type){
    $this->log( 'test_record :' . $type );

    $this->parse_record($type);
    $result = !$this->test_error();
    $this->reset_error();

    return $result;
  }

  function parse_record( $type){
    if( $this->_record_defs->valid_batch_name( $type)){
      return $this->_record_defs->unzip_record($type, $this->_current_line);
    }
    else {
      $msg = 'Unsupported recordtype '. $type .' while processing line '. $this->_current_line;

      $this->log( $msg, 'FATAL');
      $this->_error = $msg;
    }
  }

  function read_record( $type){
    $this->log( 'read_record : ' . $type );

    $result = $this->parse_record($type);
    $this->log( 'read_record : ' . print_r($result, TRUE), 'DEBUG');

    $this->read_line();
    return $result;
  }

  function setLogger( $logger){
    $this->_logger = $logger;
  }

  function log( $msg, $severity = "INFO"){
    if( isset($this->_logger)){
      $func = $this->_logger;
      $func( "$severity : $msg \n");
    }
  }

  function test_error(){
    if( isset($this->_error) && $this->_error != ''){
      return TRUE;
    }

    if( $this->_record_defs->get_error()){
      $this->_error = $this->_record_defs->get_error();
      return TRUE;
    }

    return FALSE;
  }

  function reset_error(){
    $this->set_error('');
    $this->_record_defs->reset_error();
  }

  function set_error( $msg){
    $this->_error = $msg;
  }

  function get_error(){
    return $this->_error;
  }

  function read_line(){
    if(!feof($this->_file_handle)) {
      $this->_current_line = trim(fgets($this->_file_handle, 4096));
      $this->log("line read_line: '". $this->_current_line ."'");
    }
  }
}

class verwinfo_record_defs {
  var $_error;

  function get_error(){
    return $this->_error;
  }

  function reset_error(){
    $this->_error = '';
  }

  function set_error( $msg){
    $this->_error .= $msg;
  }

  function unzip_record( $type, $line){
    $this->reset_error();

    $record_def = $this->$type();

    $data = array();

    foreach( $record_def as $field){
      $value = substr( $line, 0, $field['size']);
      if( $field['default']!= '' && $field['default']!= $value){
        $this->set_error( "Invalid value for '$type' -> '" . $field['name']. "' expected: '". $field['default'] ."' found '". $value ."'");
      }
      $line = substr( $line, $field['size']);

      $data[$field['name']]= $value;
    }
    return $data;
  }

  function zip_record_with_data( $record_def, $data= array()){
    $record = '';

    foreach($record_def as $field_def){
      $value= $data[ $field_def['name']];
      if(!isset($value)){
        $value = $field_def['default'];
      }

      if( $field_def['type'] == 'X'){
        if( $value ==''){
          $record.= str_repeat('X', $field_def['size']);
        }
        else {
          $value.= str_repeat(' ', $field_def['size']);
          $value = substr($value, 0, $field_def['size']);
          $record .= $value;
        }
      }
      else {
        $record.= substr( str_repeat('0', $field_def['size']) . $field_def['default'], -$field_def['size']);
      }
    }
    return $record;
  }

  function bestandsvoorlooprecord(){
    $result= array();

    $result[] = $this->define_field('recordcode', 'X(3)', '010');
    $result[] = $this->define_field('bestandsnaam', 'X(8)');
    $result[] = $this->define_field('filler', 'X(1)', 'A');
    $result[] = $this->define_field('bestandsversie', 'X(3)', '4.1');
    $result[] = $this->define_field('aanmaakdatum', 'X(6)');
    $result[] = $this->define_field('runnummer_bestand', 'X(4)');
    $result[] = $this->define_field('rekeningnummer_eindbestemming', 'X(10)');
    $result[] = $this->define_field('uitvoervolgnummer', 'X(4)');
    $result[] = $this->define_field('bestandsvolgnummer', 'X(2)');
    $result[] = $this->define_field('filler','X(9)');

    return $result;

  }

  function batch_voorlooprecord_1(){
    $result= array();

    $result[] = $this->define_field('recordcode', 'X(3)', '050');
    $result[] = $this->define_field('rekeningnummer_client', 'X(10)');
    $result[] = $this->define_field('filler', 'X(3)');
    $result[] = $this->define_field('totaalbedrag', 'X(18)');
    $result[] = $this->define_field('aantalposten', 'X(7)');
    $result[] = $this->define_field('testcode', 'X(1)');
    $result[] = $this->define_field('batchsoort', 'X(1)');
    $result[] = $this->define_field('periodetype', 'X(1)');
    $result[] = $this->define_field('periodelengte', 'X(2)');
    $result[] = $this->define_field('periodenummer', 'X(3)');
    $result[] = $this->define_field('filler', 'X(1)');

    return $result;
  }

  function batch_voorlooprecord_2(){
    $result= array();

    $result[] = $this->define_field('recordcode', 'X(3)', '051');
    $result[] = $this->define_field('muntsoort', 'X(3)');
    $result[] = $this->define_field('batchidentificatie', 'X(16)');
    $result[] = $this->define_field('filler', 'X(28)');

    return $result;
  }

  function post_record_1(){
    $result= array();

    $result[] = $this->define_field('recordcode', 'X(3)', '100');
    $result[] = $this->define_field('bedrag', 'X(13)');
    $result[] = $this->define_field('rekeningnummer_betaler', 'X(10)');
    $result[] = $this->define_field('rekeningnummer_begunstigde', 'X(10)');
    $result[] = $this->define_field('rekeningnummer_boeking', 'X(10)');
    $result[] = $this->define_field('controlecijfer_rekeningnummer', 'X(1)');
    $result[] = $this->define_field('betaler', 'X(1)');
    $result[] = $this->define_field('betalingskenmerkgoed', 'X(2)');

    return $result;
  }

  function eurorecord(){
    $result= array();

    $result[] = $this->define_field('recordcode', 'X(3)', '101');
    $result[] = $this->define_field('clientmuntsoort', 'X(3)');
    $result[] = $this->define_field('bedrag_nlg', 'X(13)');
    $result[] = $this->define_field('bedrag_eur', 'X(13)');
    $result[] = $this->define_field('filler', 'X(18)');

    return $result;
  }

  function post_record_2(){
    $result= array();

    $result[] = $this->define_field('recordcode', 'X(3)', '105');
    $result[] = $this->define_field('betalingskenmerk', 'X(16)');
    $result[] = $this->define_field('bronnavraaggegeven', 'X(19)');
    $result[] = $this->define_field('redenstorno', 'X(2)');
    $result[] = $this->define_field('oorspronkelijk_rekeningnummer', 'X(10)');

    return $result;
  }

  function omschrijving_record(){
    $result= array();

    $result[] = $this->define_field('recordcode', 'X(3)', '110');
    $result[] = $this->define_field('omschrijving', 'X(32)');
    $result[] = $this->define_field('filler', 'X(15)');

    return $result;
  }

  function bankinforecord(){
    $result= array();

    $result[] = $this->define_field('recordcode', 'X(3)', '115');
    $result[] = $this->define_field('bankinfo', 'X(30)');
    $result[] = $this->define_field('filler', 'X(17)');

    return $result;
  }

  function equens_record_1(){
    $result= array();

    $result[] = $this->define_field('recordcode', 'X(3)', '500');
    $result[] = $this->define_field('poststatus', 'X(2)');
    $result[] = $this->define_field('oorspronkelijkevereveningsdatum', 'X(6)');
    $result[] = $this->define_field('filler', 'X(14)');
    $result[] = $this->define_field('runnummer_post', 'X(4)');
    $result[] = $this->define_field('vereveningsdatum', 'X(6)');
    $result[] = $this->define_field('transactiesoort', 'X(4)');
    $result[] = $this->define_field('filler', 'X(11)');

    return $result;
  }

  function equens_record_3(){
    $result= array();

    $result[] = $this->define_field('recordcode', 'X(3)', '503');
    $result[] = $this->define_field('rekeningnummer', 'X(10)');
    $result[] = $this->define_field('oorspronkelijke_rekeningnummer', 'X(10)');
    $result[] = $this->define_field('filler', 'X(27)');

    return $result;
  }

  function naam_record(){
    $result= array();

    $result[] = $this->define_field('recordcode', 'X(3)', '505');
    $result[] = $this->define_field('naam', 'X(35)');
    $result[] = $this->define_field('filler', 'X(12)');

    return $result;
  }

  function straat_record(){
    $result= array();

    $result[] = $this->define_field('recordcode', 'X(3)', '510');
    $result[] = $this->define_field('straat', 'X(35)');
    $result[] = $this->define_field('filler', 'X(12)');

    return $result;
  }

  function woonplaats_record(){
    $result= array();

    $result[] = $this->define_field('recordcode', 'X(3)', '515');
    $result[] = $this->define_field('woonplaats', 'X(35)');
    $result[] = $this->define_field('filler', 'X(12)');

    return $result;
  }

  function equens_record_2(){
    $result= array();

    $result[] = $this->define_field('recordcode', 'X(3)', '600');
    $result[] = $this->define_field('signaalcode', 'X(4)');
    $result[] = $this->define_field('signaaltekst', 'X(32)');
    $result[] = $this->define_field('filler', 'X(11)');

    return $result;
  }

  function batch_sluitrecord(){
    $result= array();

    $result[] = $this->define_field('recordcode', 'X(3)', '950');
    $result[] = $this->define_field('aantalpostengeweigerd', 'X(7)');
    $result[] = $this->define_field('aantalpostengeweigerd', 'X(7)');
    $result[] = $this->define_field('aantalposten', 'X(7)');
    $result[] = $this->define_field('totaalbedrag', 'X(18)');
    $result[] = $this->define_field('filler', 'X(8)');

    return $result;
  }

  function bestandssluitrecord(){
    $result= array();

    $result[] = $this->define_field('recordcode', 'X(3)', '990');
    $result[] = $this->define_field('aantalbatches', 'X(6)');
    $result[] = $this->define_field('vervolgbatchbestandsnummer', 'X(2)');
    $result[] = $this->define_field('filler', 'X(39)');

    return $result;
  }

  function valid_batch_name( $type){
    return method_exists($this, $type);
  }

  function define_field( $name, $type_size, $default=''){
    list( $type, $size) = explode("(", $type_size);
    list( $size) = explode( ")", $size);

    if ($type == '9') {
      if($default==''){
        $default = 0;
      }
    }
    return array( 'name' => $name, 'size' => $size, 'type'=>$type, 'default'=> $default );
  }
}
?>