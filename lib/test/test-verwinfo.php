<?php
require_once "../verwinfo.class.php";

function logger( $msg){
  echo $msg;
}

//test_verwinfo();

function test_verwinfo(){

  $v = new verwinfo();
  $v->setLogger( logger);

  $v->process_file('test.txt');

  if( $v->_error){
    echo "Error while processing " . $v->_error ."\n";
    exit;
  }

  print_r( $v->_file_tree);
  
  print_r( $v->get_records_for_batch(0));
  
  exit;
}

$d = new verwinfo_record_defs();

$data_pool = array(
  'bestandsnaam' => 'bestandsnaam',
  'aanmaakdatum' => '081231',
  'batchsoort'=>'A',
  'runnummer bestand' => '9876',
  'rekeningnummer eindbestemming' => '1234567890',
  'uitvoervolgnummer' => '2341',
  'bestandsvolgnummer' => '78',
  'totaalbedrag' => '12345678.00',
  'woonplaats' => '_WOONPLAATS_',
  'omschrijving'=>'_OMSCHRIJVING_',
  'testcode' =>'T',
  'muntsoort'=> 'EUR',
  'rekeningnummer_betaler'=> '1234567890',
  'controlecijfer_rekeningnummer'=> '9',
  'clientmuntsoort' => 'USD',
  'bedrag_eur'=> '0123456789.01',
  'betalingskenmerk'=> 'BETALINGSKENMERK',
);

echo generate_file();

function generate_file(){
  $result = '';
  $result .= generate_record( 'bestandsvoorlooprecord') ."\n";
  $result .= generate_record( 'batch_voorlooprecord_1') ."\n";
  $result .= generate_record( 'batch_voorlooprecord_2') ."\n";

  $result .= generate_record( 'post_record_1') ."\n";
  $result .= generate_record( 'eurorecord') ."\n";
  $result .= generate_record( 'post_record_2') ."\n";
  $result .= generate_record( 'omschrijving_record') ."\n";
  $result .= generate_record( 'omschrijving_record') ."\n";
  $result .= generate_record( 'bankinforecord') ."\n";
  $result .= generate_record( 'equens_record_1') ."\n";
  $result .= generate_record( 'equens_record_2') ."\n";

  $result .= generate_record( 'post_record_1') ."\n";
  $result .= generate_record( 'eurorecord') ."\n";
  $result .= generate_record( 'post_record_2') ."\n";
  $result .= generate_record( 'omschrijving_record') ."\n";
  $result .= generate_record( 'omschrijving_record') ."\n";
  $result .= generate_record( 'bankinforecord') ."\n";
  $result .= generate_record( 'equens_record_1') ."\n";
  $result .= generate_record( 'equens_record_2') ."\n";

  $result .= generate_record( 'batch_sluitrecord') ."\n";
  $result .= generate_record( 'bestandssluitrecord') ."\n";

  /*
    
  $result .= generate_record( 'equens_record_3') ."\n";
  $result .= generate_record( 'naam_record') ."\n";
  $result .= generate_record( 'straat_record') ."\n";
  $result .= generate_record( 'woonplaats_record') ."\n";
  $result .= generate_record( 'equens_record_2') ."\n";
  $result .= generate_record( 'batch_sluitrecord') ."\n";
  $result .= generate_record( 'bestandssluitrecord') ."\n";
  */

  return $result;
}

function generate_record( $function){
  global $d;
  global $data_pool;

  $record_def = $d->$function();

  return $d->zip_record_with_data($record_def, $data_pool);
}

?>